import Navbar from "@/components/layouts/navbar";
import Image from "next/image";
import React, { useEffect } from 'react';

export default function Home() {

  return (
    <>
      <Navbar/>
      <section id="about" className="jumbotron">
        <div>
          <div className="main-type">
            <h1>
              Take a break enjoy your Own cup of{" "}
              <span className="text-third">Coffee</span>
            </h1>
            <ul>
              <li>
                <p>example@email.com </p>
              </li>
              <li>
                <p>st. example 283, example</p>
              </li>
              <li>
                <p>+99848344</p>
              </li>
            </ul>
          </div>
          <div className="jumbotron-image">
            <img src="../images/jumbotron/barista.png" alt="" />
          </div>
        </div>
      </section>

      <section id="menus" className="bg-third">
        <div className="container pt-32">
          <div className="flex flex-row  w-full bg-secondary rounded-t-2xl py-12 justify-between items-center">
            <ul className="flex flex-col text-light  items-end w-1/3">
              <li>Americano</li>
              <li>Espresso</li>
              <li>Cappucino</li>
            </ul>
            <div className="text-center w-1/3 text-light">
              <h1>Best Seller</h1>
            </div>
            <ul className="w-1/3 flex flex-col text-light  items-start w-1/3">
              <li>Americano</li>
              <li>Espresso</li>
              <li>Cappucino</li>
            </ul>
          </div>
          <div className="w-full flex gap-x-12">
            <div className="w-1/2 bg-primary flex flex-row justify-center items-center py-12 px-12 h-[340px] rounded-b-3xl">
              <div className="w-1/3 h-[240px] bg-gray rounded-full"></div>
              <div className="text-light w-2/3 pl-6 h-full flex justify-center items-start flex-col">
                <h2 className="mb-5">
                  Americano <span>($20)</span>
                </h2>
                <p>
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  Dolores provident dolorum numquam in illum laboriosam rem
                  reiciendis.
                </p>
              </div>
            </div>
            <div className="w-1/2 border-l-4 border-r-4 border-b-4 border-t-none border-secondary flex flex-row justify-center items-center py-12 px-12 h-[340px] rounded-b-3xl">
              <div className="w-1/3 h-[240px] bg-gray rounded-full"></div>
              <div className="text-secondary w-2/3 pl-6 h-full flex justify-center items-start flex-col">
                <h2 className="mb-5">
                  Americano <span>($20)</span>
                </h2>
                <p>
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  Dolores provident dolorum numquam in illum laboriosam rem
                  reiciendis.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
