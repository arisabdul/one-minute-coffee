import { Link, Button, Element, Events, animateScroll as scroll, scrollSpy } from 'react-scroll';
import { useEffect, useState } from "react";

const Navbar = () => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);

  useEffect(() => {
    navigationScroll();
  }, []);

  const navigationScroll = () => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  };

  const handleScroll = () => {
    const navbar = document.getElementById("navbar");
    if (navbar) {
      if (window.scrollY > 0) {
        navbar.classList.add("bg-primary");
        navbar.classList.remove("bg-transparent");
      } else {
        navbar.classList.remove("bg-primary");
        navbar.classList.add("bg-transparent");
      }
    }
  };

  const handlerMenu = () => {
    setIsOpenMenu(!isOpenMenu);
  };

  return (
    <>
      <nav className="navbar bg-transparent" id="navbar" >
        <div className="nav">
          <div>
            <ul>
              <li>
                <Link
                  to={"about"}
                  spy={true}
                  smooth={true}
                  offset={0}
                  duration={500}
                >
                  About
                </Link>
              </li>
              <li>
                <Link
                  to={"menus"}
                  spy={true}
                  smooth={true}
                  offset={0}
                  duration={500}
                >
                  Menus
                </Link>
              </li>
              <li>
                <Link href={"/"}>Services</Link>
              </li>
            </ul>
          </div>
          <div>
            <div className="text-center w-full">
              <h1>Logo</h1>
            </div>
          </div>
          <div>
            <ul>
              <li>
                <Link href={"/"} className="btn">
                  Login
                </Link>
              </li>
              <li>
                <Link href={"/"} className="btn">
                  Register
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
