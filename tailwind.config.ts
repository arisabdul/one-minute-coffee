import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    // background: radial-gradient(at top left,rgba(67,118,108,0) 20%, rgba(67,118,108,1) 40%)
    colors: {
      "primary": "#43766C",
      "secondary": "#76453B",
      "third": "#FFE455",
      "black": "#231F20",
      "light": "#F8FAE5",
      "gray": "#494949",
    },
    extend: {
      backgroundImage: {
        "jumbotron-gradient": 'radial-gradient(at bottom right,rgba(255,228,85, 0.8) 0%, rgba(67,118,108,1) 100%)',
      },
      fontFamily: {
        sans: ["Noto Sans", "sans-serif"],
        serif: ["Playfair Display", "serif"],
      },
      screens: {
        sm: "576px",
        md: "768px",
        lg: "992px",
        xl: "1200px",
        "2xl": "1400px",
        "3xl": "1600px",
        "4xl": "1920px",
      },
      container: {
        center: true,
      },
      // fontFamily: {
      //   sans: ["Roboto", "sans-serif"],
      //   "sans-serif": ["Lora", "serif"],
      //   "bebas-neue": ["Bebas Neue", "sans-serif"],
      //   "dm-serif": ["DM Serif Display", "serif"],
      // },
    },
  },
  plugins: [
    function ({ addComponents }: { addComponents: Function }) {
      addComponents({
        ".container": {
          maxWidth: "100%",
          "@screen sm": {
            maxWidth: "540px",
          },
          "@screen md": {
            maxWidth: "720px",
          },
          "@screen lg": {
            maxWidth: "960px",
          },
          "@screen xl": {
            maxWidth: "1140px",
          },
          "@screen 2xl": {
            maxWidth: "1280px",
          },
          "@screen 3xl": {
            maxWidth: "1320px",
          },
          "@screen 4xl": {
            maxWidth: "1640px",
          },
        },
      });
    },
  ],
};
export default config;
